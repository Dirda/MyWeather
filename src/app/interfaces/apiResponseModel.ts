import { responseModel } from './responseModel';
import { currentObservation } from './currentObservation';

export class apiResponseModel {
    public response: responseModel;
    public current_observation: currentObservation;
}
