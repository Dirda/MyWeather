import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { WeatherService } from '../../app/services/weather.service';

import { searchResult } from '../../app/interfaces/searchResult';

import { WeatherPage } from '../weather/weather';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage implements OnInit {

  requestError: string;

  searchResults: searchResult[];
  searchStr: string;

  defaultCity: any;

  constructor(
    public navCtrl: NavController,
    private weatherService: WeatherService) { }

  ngOnInit() {
      this.getDefaultCity();
  }

  getQuery() {
      this.weatherService.searchCities(this.searchStr)
          .subscribe(
              res => {
                this.searchResults = res.RESULTS;
            }
          );
  }

  setDefaultCity(city: searchResult) {
      this.searchResults = [];
      localStorage.setItem('location', JSON.stringify(city));
      this.searchStr = city.name;
      this.getDefaultCity();
  }

  saveChanges() {
      this.weatherService.saveNewStoredCity(this.searchStr);
      this.navCtrl.push(WeatherPage);
  }

  getDefaultCity() {
    if (localStorage.getItem('location') !== undefined) {
        this.defaultCity = JSON.parse(localStorage.getItem('location')).name
    } else {
        this.defaultCity = '';
    }
  }

}
