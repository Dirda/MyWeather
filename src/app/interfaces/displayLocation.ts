export class displayLocation {
    public city: string;
    public country: string;
    public country_iso3166: string;
    public elevation: string;
    public full: string;
    public latitude: string;
    public longitude: string;
    public magic: string;
    public state: string;
    public state_name: string;
    public wmo: string;
    public zip: string;
}
