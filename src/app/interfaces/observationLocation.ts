export class observationLocation {
    public city: string;
    public country: string;
    public country_iso3166: string;
    public elevation: string;
    public full: string;
    public latitude: string;
    public longitude: string;
    public state: string;
}
