export class searchResult {
    public name: string;
    public type: string;
    public c: string;
    public zmw: string;
    public tz: string;
    public tzs: string;
    public l: string;
    public ll: string;
    public lat: string;
    public lon: string;
}
