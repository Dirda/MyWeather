import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { WeatherService } from '../../app/services/weather.service';

import { apiResponseModel } from '../../app/interfaces/apiResponseModel';
import { searchResult } from '../../app/interfaces/searchResult';
import { currentObservation } from '../../app/interfaces/currentObservation';

@Component({
  selector: 'page-weather',
  templateUrl: 'weather.html',

})
export class WeatherPage implements OnInit {

  requestError: string;
  weather: currentObservation;
  searchStr: string;

  cityID: string;

  searchResults: searchResult[];

  constructor(
    public navCtrl: NavController,
    private weatherService: WeatherService) {
  }

  ngOnInit() {
      this.getDefaultCity();
      this.getWeather(this.cityID);
  }

  getQuery() {
      this.weatherService.searchCities(this.searchStr)
          .subscribe(
              res => {
                this.searchResults = res.RESULTS;
            }
          );
  }

  getWeather(cityID: string) {
      this.weatherService.getWeather(cityID)
          .subscribe((data: apiResponseModel) => {
              console.log(data);
              this.weather = data.current_observation;
          },
          err => {
              this.requestError = err.json().errors;
          });
  }

  getDefaultCity() {
    if (localStorage.getItem('location') !== undefined) {
        this.cityID = JSON.parse(localStorage.getItem('location')).zmw
    } else {
        this.cityID = this.weatherService.storedCity;
    }
    console.log(this.cityID);
  }

  chooseCity(city: searchResult) {
      this.searchResults = [];
      this.cityID = city.zmw;
      this.getWeather(this.cityID);
  }

}
