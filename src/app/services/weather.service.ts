import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class WeatherService {
    http: any;
    apiKey: string;
    conditionsUrl: string;
    searchUrl: string;

    storedCity: string;

    constructor(
      http: Http
    ) {
        this.http = http;
        this.apiKey = 'b7199a1d970f3179';
        this.conditionsUrl = 'http://api.wunderground.com/api/' + this.apiKey + '/conditions/q';
        this.searchUrl = 'http://localhost:8100/search/aq?query=';
    }

    getWeather(zmw: string) {
        return this.http.get(this.conditionsUrl + '/zmw:' + zmw +'.json')
          .map((res: Response) => res.json());
    }

    searchCities(searchParam: string) {
        return this.http.get(this.searchUrl + searchParam)
          .map((res: Response) => res.json());
    }

    saveNewStoredCity(city: string) {
        this.storedCity = city;
    }
}
